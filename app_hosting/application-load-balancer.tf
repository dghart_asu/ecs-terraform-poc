resource "aws_alb" "ecs_load_balancer" {
    name                = "ecs-load-balancer"
    security_groups     = [aws_security_group.demo_public_sg.id]
    subnets             = [aws_subnet.test_public_sn_01.id, aws_subnet.test_public_sn_02.id]

    tags = {
      Name = "ecs-load-balancer"
    }
}

resource "aws_alb_target_group" "ecs_target_group" {
    name                = "ecs-target-group"
    port                = "80"
    protocol            = "HTTP"
    vpc_id              = aws_vpc.my_vpc.id

    health_check {
        healthy_threshold   = "5"
        unhealthy_threshold = "2"
        interval            = "30"
        matcher             = "200"
        path                = "/"
        port                = "traffic-port"
        protocol            = "HTTP"
        timeout             = "5"
    }

    tags = {
      Name = "ecs-target-group"
    }
}

resource "aws_alb_listener" "alb_listener" {
    load_balancer_arn = aws_alb.ecs_load_balancer.arn
    port              = "80"
    protocol          = "HTTP"

    default_action {
        target_group_arn = aws_alb_target_group.ecs_target_group.arn
        type             = "forward"
    }
}