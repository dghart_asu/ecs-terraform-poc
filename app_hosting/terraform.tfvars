# http://blog.shippable.com/create-a-container-cluster-using-terraform-with-aws-part-1
# http://blog.shippable.com/setup-a-container-cluster-on-aws-with-terraform-part-2-provision-a-cluster

ecs_cluster="test-ecs-cluster"
ecs_key_pair_name="asu-ssh-keypair"
region = "us-west-2"
test_vpc = "$TEST_VPC" # not used
test_network_cidr = "200.0.0.0/16"
test_public_01_cidr = "200.0.1.0/24"
test_public_02_cidr = "200.0.2.0/24"
max_instance_size = "4"
min_instance_size = "2"
desired_capacity = "2"