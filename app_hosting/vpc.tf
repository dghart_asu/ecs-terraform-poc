provider "aws" {
  version = "~> 2.0"
  region  = "us-west-2"
}

resource "aws_vpc" "my_vpc" {
    cidr_block = "200.0.0.0/16"
    tags = {
        Name = "ecsDemoVPC"
    }
}

resource "aws_internet_gateway" "demo_gw" {
    vpc_id = aws_vpc.my_vpc.id
    tags = {
        Name = "ecsDemoGateway"
    }
}

resource "aws_subnet" "test_public_sn_01" {
    vpc_id = aws_vpc.my_vpc.id
    cidr_block = var.test_public_01_cidr
    availability_zone = "us-west-2a"
    tags = {
        Name = "test_public_sn_01"
    }
}

resource "aws_route_table" "test_public_sn_01_rt" {
    vpc_id = aws_vpc.my_vpc.id
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.demo_gw.id
    }
    tags = {
        Name = "test_public_sn_01_rt"
    }
}

resource "aws_route_table_association" "test_public_sn_01_assoc" {
    subnet_id = aws_subnet.test_public_sn_01.id
    route_table_id = aws_route_table.test_public_sn_01_rt.id
}

resource "aws_subnet" "test_public_sn_02" {
    vpc_id = aws_vpc.my_vpc.id
    cidr_block = var.test_public_02_cidr
    availability_zone = "us-west-2b"
    tags = {
        Name = "test_public_sn_02"
    }
}

resource "aws_route_table" "test_public_sn_02_rt" {
    vpc_id = aws_vpc.my_vpc.id
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.demo_gw.id
    }
    tags = {
        Name = "test_public_sn_02_rt"
    }
}

resource "aws_route_table_association" "test_public_sn_02_assoc" {
    subnet_id = aws_subnet.test_public_sn_02.id
    route_table_id = aws_route_table.test_public_sn_02_rt.id
}

resource "aws_security_group" "demo_public_sg" {
    name = "demo_public_sg"
    description = "Security Group allowing public access to the VPC"
    vpc_id = aws_vpc.my_vpc.id

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 0
        to_port = 0
        protocol = "tcp"
        cidr_blocks = [
         var.test_public_01_cidr,
         var.test_public_02_cidr]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = { 
        Name = "demo_public_sg"
    }
}